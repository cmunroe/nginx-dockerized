Make sure to run:
```
docker network create --ipv6 --subnet=fd00:dddd:dddd::/64 www
```

Edit `/etc/docker/daemon.json`

```
"userland-proxy": false, "ipv6": true, "fixed-cidr-v6": "fd00:c0fe:babe::/48"
```

More information: https://github.com/nginx-proxy/nginx-proxy/issues/1419


# Auto Updates / Cleaning

## WatchTower (auto updates)
```
docker run -d --restart always --name watchtower   -v /root/.docker/config.json:/config.json   -v /var/run/docker.sock:/var/run/docker.sock   containrrr/watchtower -i 300 --cleanup
```

## Docker-Cleaner (cleaning)
```
docker run -d --restart always   -v /var/run/docker.sock:/var/run/docker.sock:rw   -v /var/lib/docker:/var/lib/docker:rw   meltwater/docker-cleanup:latest
```
